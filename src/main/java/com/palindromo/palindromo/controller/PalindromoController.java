package com.palindromo.palindromo.controller;

import com.palindromo.palindromo.palindromo.Contracts.IValidadorPalindromo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@CrossOrigin
@RestController
@RequestMapping(value = "/auth/")
public class PalindromoController {

	@Autowired
	IValidadorPalindromo validadorPalindromo;

	@PostMapping(value = "/palindromo")
	public ResponseEntity<String> save(@RequestBody String texto) {

		return new ResponseEntity<String>(validadorPalindromo.getLargestPalindrome(texto), HttpStatus.OK);
	}

	@GetMapping(value = "/palindromo/{texto}")
	public ResponseEntity<String> find(@PathVariable String texto) {		

		return new ResponseEntity<String>(validadorPalindromo.getLargestPalindrome(texto), HttpStatus.OK);
	}
}
