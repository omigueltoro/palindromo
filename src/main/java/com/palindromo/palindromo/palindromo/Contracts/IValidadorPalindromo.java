package com.palindromo.palindromo.palindromo.Contracts;

public interface IValidadorPalindromo {
    String getLargestPalindrome(String texto);
}
