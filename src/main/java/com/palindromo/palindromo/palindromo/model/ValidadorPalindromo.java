package com.palindromo.palindromo.palindromo.model;

import com.palindromo.palindromo.palindromo.Contracts.IValidadorPalindromo;

import org.springframework.stereotype.Component;


@Component("validadorPalindromo")
public class ValidadorPalindromo implements IValidadorPalindromo {


    public String getLargestPalindrome(String texto) {

        boolean activeImpar = false;
        boolean activePar = false;

        //A medida que se validan letras iguales el indice inicial debe retrocedes para validar las siguientes letras
        int retroceso = 1;

        //indices del palindromo mas largo encontrado
        int beginindicePalindromo = 0;
        int endindicePalindromo = 0;

        //Recorre cadena de caracteres
        for (int indiceFinal = 1; indiceFinal < texto.length(); indiceFinal++) {

            //Valida que la letra del indice inicial con la letra del indice Final, si no esta activa una validación de palindromo impar
            if (texto.charAt(indiceFinal - retroceso) == texto.charAt(indiceFinal) && !activeImpar) {
                activePar = true;

                // Si el largo del palindromo es mas grande que uno anteriormente encontrado, se realiza el guardado de estos indices
                if ((retroceso) > (endindicePalindromo - beginindicePalindromo)) {
                    beginindicePalindromo = indiceFinal - retroceso;
                    endindicePalindromo = indiceFinal;
                }
                // Si el indiceFinal - retroceso es mayor igual a 3 puede aumentar su retroceso
                if (indiceFinal - retroceso >= 3){
                    retroceso = retroceso + 2;
                }
            } 

            //Valida que la letra del indice inicial saltando una letra con la letra del indice Final, 
            //para validar palindromos impares, todo esto solo sí no esta activa una validación de palindromo par
            else if (texto.charAt(indiceFinal - retroceso - (indiceFinal < 2 ? 0 : 1)) == texto.charAt(indiceFinal) && indiceFinal >= 2 && !activePar) {
                activeImpar = true;

                // Si el largo del palindromo es mas grande que uno anteriormente encontrado, se realiza el guardado de estos indices
                if ((retroceso + 1) > (endindicePalindromo - beginindicePalindromo)) {
                    beginindicePalindromo = indiceFinal - retroceso - 1;
                    endindicePalindromo = indiceFinal;

                }
                // Si el indiceFinal - retroceso es mayor igual a 3 puede aumentar su retroceso
                if (indiceFinal - retroceso >= 3)
                {
                    retroceso = retroceso + 2;
                }

            } 
            // Si no cumple el caracter para que la subcadena sea palindromo se desactivan las validaciones y se restablecen los valores
            else {
                if (activeImpar || activePar) {
                    indiceFinal--;
                }
                activeImpar = false;
                activePar = false;
                retroceso = 1;
            }
        }
        return texto.substring(beginindicePalindromo, endindicePalindromo + 1);
    }
}
