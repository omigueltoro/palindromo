package com.palindromo.palindromo;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.palindromo.palindromo.palindromo.Contracts.IValidadorPalindromo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PalindromoApplicationTests {

	@Autowired
	IValidadorPalindromo validadorPalindromo;

	@Test
	void contextLoads() {

		var largestPalindrome = validadorPalindromo.getLargestPalindrome("asa");

		StringBuilder builder=new StringBuilder(largestPalindrome);
		
		String InvertString = builder.reverse().toString();

		assertTrue(InvertString == largestPalindrome);
	}

}
